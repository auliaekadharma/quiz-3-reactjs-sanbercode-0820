import React from 'react'

class About extends React.Component {
	render() {
		
		const divstyle = {
			padding: "10px",
			border: "1px solid #ccc"
		};

		const h1style = {
			textAlign: "center"
		};

		const listyle = {
			width: "100ox"
		};

		return (
			<>
			<div style={divstyle}>
				<h1 style={h1style}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
				<ol>
					<li><strong style={listyle}>Nama:</strong> Aulia Eka Dharma</li> 
					<li><strong style={listyle}>Email:</strong> auliaekadharma@gmail.com</li> 
					<li><strong style={listyle}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
					<li><strong style={listyle}>Akun Gitlab:</strong> auliaekadharma</li> 
					<li><strong style={listyle}>Akun Telegram:</strong> Aulia Eka Dharma</li> 
				</ol>
			</div>
			<br/>
			<br/>
			<a href="index.html">
				<button>kembali ke index</button>
			</a>
			</>
		)
	}
}

export default About