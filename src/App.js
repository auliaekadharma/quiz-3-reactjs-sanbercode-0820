import React from 'react';
import logo from './logo.png'
import './style.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import About from './Quiz/About'


function App() {
  return (
    <>
    <header>
      <img id="logo" src={logo} width="200px" />
      <Router>
      <nav>
        <ul>
          <li> <Link to="/">Home</Link> </li>
          <li> <Link to="/about">About</Link> </li>
          <li> <Link to="/movieListEditor">Movie List Editor</Link> </li>
          <li> <Link to="/login">Login</Link> </li>
        </ul>
      </nav>
      <Switch>
        <Route path="/login">
          {/* <Login /> */}
        </Route>
        <Route path="/movieListEditor">
          {/* <MovieListEditor /> */}
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/">
          {/* <Home /> */}
        </Route>
      </Switch>
      
      </Router>
    </header>

    <section>
      <h1>Featured Posts</h1>
            <div id="article-list">
              <div class="article">
                <a href=""><h3>Lorem Post 1</h3></a>
                <p>
                  Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
              </div>
              <div class="article">
                <a href=""><h3>Lorem Post 2</h3></a>
                <p>
                  Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
              </div>
              <div class="article">
                <a href=""><h3>Lorem Post 3</h3></a>
                <p>
                  Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
              </div>
              <div class="article">
                <a href=""><h3>Lorem Post 4</h3></a>
                <p>
                  Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
              </div>
              <div class="article">
                <a href=""><h3>Lorem Post 5</h3></a>
                <p>
                  Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
              </div>
              <div>
                  <a href=""><h3>Lorem Post 5</h3></a>
                  <p>
                    Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                  </p>
                </div>
                <div>
                  <a href=""><h3>Lorem Post 5</h3></a>
                  <p>
                    Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                  </p>
                </div>
            </div>
          </section>
          <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
          </footer>
    </>
  );
}

export default App;
